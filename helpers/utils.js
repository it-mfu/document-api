/**
 * Created by sherlock on 8/5/2017 AD.
 */
var cfg = require('../config/config');
var logger = require('./logger');
var crypto = require('crypto');
var mime = require('mime');
var macaddress = require('macaddress');
// var key = "joylife";

exports.writeLog = function (method, req, startTime, res, err) {
    if (cfg.debug) {
        if (req.body.staffObj) {
            req.body.staffObj = req.body.staffObj.email;
        }
        var logString = '';
        var stopTime = this.getTimeInMsec();
        logString = appendLog(logString, '----- Start ' + method + ' -----');
        logString = appendLog(logString, '----- incomming parameters Headers [' + JSON.stringify(req.headers) + '] -----');
        logString = appendLog(logString, '----- incomming parameters Body [' + JSON.stringify(req.body) + '] -----');
        logString = appendLog(logString, '----- request ip [' + req.ip + '] -----');
        logString = appendLog(logString, '----- ResponseMessage[' + JSON.stringify(res) + '] ResponseTime[' + diffTimeInMsec(startTime, stopTime) + '] -----');
        if (err) {
            logString = appendLog(logString, '----- ErrorMessage[' + err + '] -----');
        }
        logger.writeInfo(logString);
    }
}

exports.writeLog_Payment = function (method, req, startTime, res, err) {
    if (cfg.debug) {
        if (req.body.staffObj) {
            req.body.staffObj = req.body.staffObj.email;
        }
        var logString = '';
        var stopTime = this.getTimeInMsec();
        logString = appendLog(logString, '----- Start ' + method + ' -----');
        logString = appendLog(logString, '----- incomming parameters Headers [' + JSON.stringify(req.headers) + '] -----');
        logString = appendLog(logString, '----- incomming parameters Body [' + JSON.stringify(req.body) + '] -----');
        logString = appendLog(logString, '----- request ip [' + req.ip + '] -----');
        logString = appendLog(logString, '----- ResponseMessage[' + JSON.stringify(res) + '] ResponseTime[' + diffTimeInMsec(startTime, stopTime) + '] -----');
        if (err) {
            logString = appendLog(logString, '----- ErrorMessage[' + err + '] -----');
        }
        logger.writeInfo(logString);
    }
}

exports.getTimeInMsec = function () {
    return (new Date().getTime());
}

exports.TimeInMsecToDate = function (datetime) {
    var dates = new Date(datetime);
    var years = dates.getFullYear();
    var months = dates.getMonth() + 1;
    var day = dates.getDate();
    var hours = dates.getHours();
    var minutes = dates.getMinutes();
    var second = new Date().getSeconds();
    var millisecs = dates.getMilliseconds();
    var monthFormatted = months < 10 ? "0" + months : months;
    var dayFormatted = day < 10 ? "0" + day : day;
    var hourFormatted = hours < 10 ? "0" + hours : hours;
    var minFormatted = minutes < 10 ? "0" + minutes : minutes;
    var secFormatted = second < 10 ? "0" + second : second;
    var milliFormatted = null;

    if (millisecs < 10) {
        milliFormatted = "00" + millisecs;
    } else if (millisecs < 100) {
        milliFormatted = "0" + millisecs;
    } else {
        milliFormatted = millisecs;
    }
    dates = null;
    return '[' + years + '-' + monthFormatted + '-' + dayFormatted + ' ' + hourFormatted + ':' + minFormatted + ':' + secFormatted + ':' + milliFormatted + ']';
}


exports.createToken = function (cb) {
    crypto.randomBytes(cfg.tokenLength, function (err, token) {
        if (err) {
            cb(err, null);
        } else {
            cb(null, token.toString('hex'));
        }
    });
}
exports.checkMacAddress = function (data) {
    macaddress.one(function (err, mac) {
        (err)? data(error, null):data(null, mac);
    });
}

exports.randomString = function(nummber){
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < nummber; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

exports.randomNumber = function(nummber){
    var text = "";
    var possible = "0123456789";

    for (var i = 0; i < nummber; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

exports.createTransaction = function (cb) {
    crypto.randomBytes(cfg.tokenLength, function (err, token) {
        if (err) {
            cb(err, null);
        } else {
            cb(null, token.toString('hex'));
        }
    });
}

function appendLog(src, dest) {
    if (src == '')
        src = '';
    return (src + getDateTimeLogFormat() + ' - info: ' + dest + '\n');
}

function diffTimeInMsec(startTime, stopTime) {
    return (stopTime - startTime);
}

function getDateTimeLogFormat() {
    var dates = new Date();
    var years = dates.getFullYear();
    var months = dates.getMonth() + 1;
    var day = dates.getDate();
    var hours = dates.getHours();
    var minutes = dates.getMinutes();
    var second = new Date().getSeconds();
    var millisecs = dates.getMilliseconds();
    var monthFormatted = months < 10 ? "0" + months : months;
    var dayFormatted = day < 10 ? "0" + day : day;
    var hourFormatted = hours < 10 ? "0" + hours : hours;
    var minFormatted = minutes < 10 ? "0" + minutes : minutes;
    var secFormatted = second < 10 ? "0" + second : second;
    var milliFormatted = null;

    if (millisecs < 10) {
        milliFormatted = "00" + millisecs;
    } else if (millisecs < 100) {
        milliFormatted = "0" + millisecs;
    } else {
        milliFormatted = millisecs;
    }
    dates = null;
    return '[' + years + '-' + monthFormatted + '-' + dayFormatted + ' ' + hourFormatted + ':' + minFormatted + ':' + secFormatted + ':' + milliFormatted + ']';
}

exports.diffDays = function (dateEnd, DateStart) {
    var date1 = new Date(dateEnd);
    var date2 = new Date(DateStart);
    var diffDays = parseInt((date2 - date1) / (1000 * 60 * 60 * 24)); //gives day difference
//one_day means 1000*60*60*24
//one_hour means 1000*60*60
//one_minute means 1000*60
//one_second means 1000
    return diffDays;
}

exports.encrypt = function (data) {
    var cipher = crypto.createCipher('aes-256-cbc', key);
    var crypted = cipher.update(data, 'utf-8', 'hex');
    crypted += cipher.final('hex');

    return crypted;
}

exports.decrypt = function (data) {
    var decipher = crypto.createDecipher('aes-256-cbc', key);
    var decrypted = decipher.update(data, 'hex', 'utf-8');
    decrypted += decipher.final('utf-8');

    return decrypted;
}


exports.sendEmail = function (bookName, message,email, url, callback) {
    var nodemailer = require('nodemailer');
    //// create reusable transporter object using the default SMTP transport
    var transporter = nodemailer.createTransport('smtps://' + cfg.accountGmail.username + ':' + cfg.accountGmail.password + '@smtp.gmail.com');
    var msg = "<div>Service</div>";
    msg += "<br/>";
    // msg += '<div>Review book: ' + bookName + '</div>';
    // msg += "<br/>";
    msg += '<div> OTP code : ' + message + '</div>';
    msg += '<div> You shall put code to system verlification service</div>';
    msg += "<br/>";
    // msg += '<div>Please click link for approve review</div>';
    // msg += "<br/>";
    // msg += "<div><a href='" + url + "Y'>Approve</a> &nbsp;&nbsp;<a href='" + url + "N'>Reject</a></div>";

    var mailOptions = {
        from: '"ThaiGpsTracker Service" '+cfg.accountGmail.username, // sender address
        // to: 'atthapon.kr@gmail.com', // list of receivers
        // to: 'support@jarvissolution.com', // list of receivers
        to:email,
        subject: '[Service] Vertify Code ', // Subject line
        html: msg
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            callback(error, null);
        }
        callback(null, info);
    });
}

exports.sendSms = function(message,phone){
    var request = require("request");

    var options = { method: 'POST',
        url: 'http://www.thaibulksms.com/sms_api.php',
        headers:
            { 'postman-token': '623dbd93-b95b-7e72-3434-ea767e5c037e',
                'cache-control': 'no-cache',
                'content-type': 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' },
        formData:
            { username: '0934102002',
                password: '826690',
                msisdn: phone,
                message: 'OTP Code :'+message,
                // sender: 'sms'
                }};
    request(options, function (error, response, body) {
        if (error) throw new Error(error);
        // console.log(response);
    });

}

var key = '772841011E88501E4BAC038248FA30F5A5F3B11E3315E17CE3314918052C1338';
var data = '7.27647640000013592 days 1 night hotel room1529462619702000000002500http://192.168.64.2/payment/result.phpPayment ';

exports.getHash = function(){
    console.log("ssss");
    var hmac = crypto.createHmac('sha1', key);
    hmac.update(data);

    console.log(hmac.digest('binary'));


    return hmac.digest('binary');
}




exports.encrypts = function () {
    var cipher = crypto.createHmac('sha1', key);
    var crypted = cipher.update(data, 'utf-8');
    crypted += cipher.final('hex');

    return crypted;
}