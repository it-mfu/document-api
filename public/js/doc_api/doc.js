$(document).ready(function() {
    var data = {"data":{
        "apiProject":[{
            "id":"1",
            "name":"Doc",
            "apiList":[
                {
                    "id":"1",
                    "name":"Get Group Doc",
                    "description":"this api support group service for dormmitory open",
                    "createDate":"2018-10-02 09:17:34.911Z",
                    "lastUpdate":"2018-10-23 09:17:34.911Z",
                    "revokedate":"2018-10-23 09:17:34.911Z",
                    "content":{
                        "protocal":"http",
                        "method":"POST",
                        "url":[
                            {
                                "iot":"http://10.1.4.81:901/student/dorm",
                                "live":"http://mfu:8091/student/dorm"
                            }
                        ],
                        "requestUrlParameters":[
                        ],
                        "requestHeadersParameters":[
                            {
                                "parameterName":"Authorization",
                                "require":"M",
                                "valueType":"SV",
                                "dataType":"String",
                                "description":"Authorization Key Access"
                            }
                        ],
                        "requestBodyParameters":[
                            {
                                "parameterName":"StudentId",
                                "require":"M",
                                "valueType":"SV",
                                "dataType":"String",
                                "description":"602902010"
                            }
                        ],

                        "requestExsample":{
                            "attributes":{
                                "friendly_name":"Bed Room",
                                "supported_features":1
                            },
                            "entity_id":"light.bed_room",
                            "last_changed":"2018-03-22T03:13:22.143324+00:00",
                            "last_updated":"2018-03-22T03:13:22.143324+00:00",
                            "state":"off"
                        },

                        "responseBody":{
                            "attributes":{
                                "friendly_name":"Bed Room",
                                "supported_features":1
                            },
                            "entity_id":"light.bed_room",
                            "last_changed":"2018-03-22T03:13:22.143324+00:00",
                            "last_updated":"2018-03-22T03:13:22.143324+00:00",
                            "state":"off"
                        }
                    },
                    "status":"ready",
                    "note":""
                },{
                    "id":"2",
                    "name":"Get Group Service",
                    "description":"this api support group service for dormmitory open",
                    "createDate":"2018-10-02 09:17:34.911Z",
                    "lastUpdate":"2018-10-23 09:17:34.911Z",
                    "revokedate":"2018-10-23 09:17:34.911Z",
                    "content":{
                        "protocal":"http",
                        "method":"POST",
                        "url":[
                            {
                                "iot":"http://10.1.4.81:901/student/dorm",
                                "live":"http://mfu:8091/student/dorm"
                            }
                        ],
                        "requestUrlParameters":[
                        ],
                        "requestHeadersParameters":[
                            {
                                "parameterName":"Authorization",
                                "require":"M",
                                "valueType":"SV",
                                "dataType":"String",
                                "description":"Authorization Key Access"
                            }
                        ],
                        "requestBodyParameters":[
                            {
                                "parameterName":"StudentId",
                                "require":"M",
                                "valueType":"SV",
                                "dataType":"String",
                                "description":"602902010"
                            }
                        ],

                        "requestExsample":{
                            "attributes":{
                                "friendly_name":"Bed Room",
                                "supported_features":1
                            },
                            "entity_id":"light.bed_room",
                            "last_changed":"2018-03-22T03:13:22.143324+00:00",
                            "last_updated":"2018-03-22T03:13:22.143324+00:00",
                            "state":"off"
                        },

                        "responseBody":{
                            "attributes":{
                                "friendly_name":"Bed Room",
                                "supported_features":1
                            },
                            "entity_id":"light.bed_room",
                            "last_changed":"2018-03-22T03:13:22.143324+00:00",
                            "last_updated":"2018-03-22T03:13:22.143324+00:00",
                            "state":"off"
                        }
                    },
                    "status":"ready",
                    "note":""
                }
            ]
        },{
            "id":"2s",
            "name":"Dormmitory",
            "apiList":[
                {
                    "id":"3",
                    "name":"Get Group Service",
                    "description":"this api support group service for dormmitory open",
                    "createDate":"2018-10-02 09:17:34.911Z",
                    "lastUpdate":"2018-10-23 09:17:34.911Z",
                    "revokedate":"2018-10-23 09:17:34.911Z",
                    "content":{
                        "protocal":"http",
                        "method":"POST",
                        "url":[
                            {
                                "iot":"http://10.1.4.81:901/student/dorm",
                                "live":"http://mfu:8091/student/dorm"
                            }
                        ],
                        "requestUrlParameters":[
                        ],
                        "requestHeadersParameters":[
                            {
                                "parameterName":"Authorization",
                                "require":"M",
                                "valueType":"SV",
                                "dataType":"String",
                                "description":"Authorization Key Access"
                            }
                        ],
                        "requestBodyParameters":[
                            {
                                "parameterName":"StudentId",
                                "require":"M",
                                "valueType":"SV",
                                "dataType":"String",
                                "description":"602902010"
                            }
                        ],

                        "requestExsample":{
                            "attributes":{
                                "friendly_name":"Bed Room",
                                "supported_features":1
                            },
                            "entity_id":"light.bed_room",
                            "last_changed":"2018-03-22T03:13:22.143324+00:00",
                            "last_updated":"2018-03-22T03:13:22.143324+00:00",
                            "state":"off"
                        },

                        "responseBody":{
                            "attributes":{
                                "friendly_name":"Bed Room",
                                "supported_features":1
                            },
                            "entity_id":"light.bed_room",
                            "last_changed":"2018-03-22T03:13:22.143324+00:00",
                            "last_updated":"2018-03-22T03:13:22.143324+00:00",
                            "state":"off"
                        }
                    },
                    "status":"ready",
                    "note":""
                },{
                    "id":"4",
                    "name":"Get Group Create",
                    "description":"this api support group service for dormmitory open",
                    "createDate":"2018-10-02 09:17:34.911Z",
                    "lastUpdate":"2018-10-23 09:17:34.911Z",
                    "revokedate":"2018-10-23 09:17:34.911Z",
                    "content":{
                        "protocal":"http",
                        "method":"POST",
                        "url":[
                            {
                                "iot":"http://10.1.4.81:901/student/dorm",
                                "live":"http://mfu:8091/student/dorm"
                            }
                        ],
                        "requestUrlParameters":[
                        ],
                        "requestHeadersParameters":[
                            {
                                "parameterName":"Authorization",
                                "require":"M",
                                "valueType":"SV",
                                "dataType":"String",
                                "description":"Authorization Key Access"
                            }
                        ],
                        "requestBodyParameters":[
                            {
                                "parameterName":"StudentId",
                                "require":"M",
                                "valueType":"SV",
                                "dataType":"String",
                                "description":"602902010"
                            }
                        ],

                        "requestExsample":{
                            "attributes":{
                                "friendly_name":"Bed Room",
                                "supported_features":1
                            },
                            "entity_id":"light.bed_room",
                            "last_changed":"2018-03-22T03:13:22.143324+00:00",
                            "last_updated":"2018-03-22T03:13:22.143324+00:00",
                            "state":"off"
                        },

                        "responseBody":{
                            "attributes":{
                                "friendly_name":"Bed Room",
                                "supported_features":1
                            },
                            "entity_id":"light.bed_room",
                            "last_changed":"2018-03-22T03:13:22.143324+00:00",
                            "last_updated":"2018-03-22T03:13:22.143324+00:00",
                            "state":"off"
                        }
                    },
                    "status":"ready",
                    "note":""
                }
            ]
        }]
    }};
    onListApi();

    var  apiService = new myClass();
    apiService.construct(data);


    function onListApi(){
        $('.menu').html();
        var dataText = "";
        dataText += '<li class="submenu">';
        var apiProject = data.data.apiProject;
        for(i=0; i<apiProject.length;i++){
            dataText += '<a apiProjectId="'+apiProject[i].id+'"><i class="fa fa-fw fa-tv"></i> <span>'+apiProject[i].name+'</span> <span class="menu-arrow"></span></a>';

            dataText += '<ul class="list-unstyled">';
            for(j=0;j<apiProject[i].apiList.length; j++){
                var info = apiProject[i].apiList[j];
                dataText += '<li class="list"><a value="'+info.id+'">'+info.name+'</a></li>';
            }
            dataText += '</ul>';
        }
        dataText += '</li>';

        $('.menu').append(dataText);
    }

    $(document).on('click', 'li a', function() {

        var apiProjectId = $(this).attr('apiProjectId');
        var val = $(this).attr('value');
        var classs = $(this).parent().hasClass("submenu");
        if(classs == true){
            $('.docApi').hide();
            $('.apilist').show();
            $('ul.list-unstyled').css("display","none");
            $('.subdrop').removeClass('subdrop');
            $(this).addClass('subdrop');
            apiService.onByApiList(apiProjectId)

        }else{
            $('.apilist').hide();
            $('.docApi').show();
            $('li.list a.subdrop').removeClass('subdrop');
            $(this).addClass('subdrop');
            apiService.onByContentAPI(val);
        }
        $(this).next().css("display","block");



    });




});