var myClass = function(options){
    /*
     * Variables accessible
     * in the class
     */
    var data = {};

    /*
     * Can access this.method
     * inside other methods using
     * root.method()
     */
    var root = this;

    /*
     * Constructor
     */
    this.construct = function(options){
        $.extend(data , options);
    };

    /*
     * Public method
     * Can be called outside class
     */
    this.myPublicMethod = function(){
        return data;
    };

    /*
     * Private method
     * Can only be called inside class
     */
    var myPrivateMethod = function() {
        console.log('accessed private method');
    };


    /*
     * Pass options when class instantiated
     */
    this.construct(options);

    this.onByApiList = function(apiProjectId){
        var apiProject = data.data.apiProject;
        for(i=0; i<apiProject.length;i++){
            if(apiProject[i].id == apiProjectId){
                setViewListApi(apiProject[i]);
            }
        }
    };

    function setViewListApi(data){
        console.log(data);
        $('.projectname').html(data.name);
        $('.list-of-api').html('');
        var apilist = data.apiList;
        for(i=0;i<apilist.length;i++){
            var info = apilist[i];
            var dataText ='<tr>' +
                '<th scope="row" class="text-center">'+(i+1)+'</th>' +
                '<td class="text-center">'+info.content.method+'</td>' +
                '<td>'+info.name+'</td>' +
                '<td>'+info.description+'</td>' +
                '<td>'+info.lastUpdate+'</td>' +
                '<td>'+info.revokedate+'</td>' +
                '<td>'+info.status+'</td>' +
                '</tr>';
            $('.list-of-api').append(dataText);

        }

    };

    this.onByContentAPI = function onSetView(apiNumber){

        var apiProject = data.data.apiProject;
        for(i=0; i<apiProject.length;i++){
            for(j=0;j<apiProject[i].apiList.length; j++){
                var info = apiProject[i].apiList[j];
                if(info.id == apiNumber){
                    setViewApiContent(info);
                }
            }
        }
    };

    var setViewApiContent = function (data) {

        var info = data;
        $('.doc .name').text(info.name);
        $('.doc .description').text(info.description);

        var contentURL = info.content.requestHeadersParameters;
        for(u=0;u<contentURL.length; u++){
            var urls = contentURL[u];
            var dataText ='<tr><td>'+urls.parameterName+'</td><td class="text-center">'+urls.require+'</td><td class="text-center">'+urls.valueType+'</td><td>'+urls.dataType+'</td><td>'+urls.description+'</td></tr>';
            $('.header-Parameters').html(dataText);
        }

        var contentHeader = info.content.requestHeadersParameters;
        for(h=0;h<contentHeader.length; h++){
            var headers = contentHeader[h];
            var dataText ='<tr><td>'+headers.parameterName+'</td><td class="text-center">'+headers.require+'</td><td class="text-center">'+headers.valueType+'</td><td>'+headers.dataType+'</td><td>'+headers.description+'</td></tr>';
            $('.header-Parameters').html(dataText);
        }

        var contentBody = info.content.requestBodyParameters;
        for(b=0;b<contentBody.length; b++){
            var bodys = contentHeader[b];
            var dataText ='<tr><td>'+bodys.parameterName+'</td><td class="text-center">'+bodys.require+'</td><td class="text-center">'+bodys.valueType+'</td><td>'+bodys.dataType+'</td><td>'+bodys.description+'</td></tr>';
            $('.body-Parameters').html(dataText);
        }

        var requestExsample = info.content.requestExsample;
        document.getElementById("requestExsample").innerHTML = JSON.stringify(requestExsample, undefined, 2);

        var responseBody = info.content.responseBody;
        document.getElementById("responseBody").innerHTML = JSON.stringify(responseBody, undefined, 2);
    }
};