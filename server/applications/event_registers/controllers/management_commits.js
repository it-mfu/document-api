var utils = require('../../../../helpers/utils');
var configs = require('../../../../config/config');
var resMsgs = require('../../management_messages/controllers/management_messages');
var mongo = require('mongodb');
var isodate = require("isodate");

var commits = require('../models/commits.model');

exports.onEvents = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onEventRegisters";
    try {
        var query = {};
        query.enable = true;

        commits
            .find(query)
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0, 50002, function (res) {
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0, 40100, function (res) {
                            response.status(401).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            })
    }catch (err) {
        resMsgs.onMessage_Response(0,40300,function(res){
            response.status(403).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}

exports.onEvent = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onEventRegisters";
    try {
        var query = {};
        (request.params.id != null) ? query.code = request.params.id : null;
        query.enable = true;

        commits
            .find(query)
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0, 50002, function (res) {
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0, 40100, function (res) {
                            response.status(401).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            })
    }catch (err) {
        resMsgs.onMessage_Response(0,40300,function(res){
            response.status(403).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}

exports.onEvent_Create = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onEvent_Create";
    try {
        var data = {};
        (request.body.title != null) ? data.title = request.body.title : null;
        (request.body.subtitle != null) ? data.subtitle = request.body.subtitle : null;
        (request.body.description != null) ? data.description = request.body.description : null;
        data.code = utils.randomString(4)+startTime;

        (request.body.dateTime != null) ? data.dateTime = request.body.dateTime : null;

        (request.body.onlyRegistered != null) ? data.onlyRegistered = request.body.onlyRegistered : null;
        (request.body.maxRegistered != null) ? data.maxRegistered = request.body.maxRegistered : null;
        (request.body.notification != null) ? data.notification = request.body.notification : null;
        (request.body.registered != null) ? data.registered = request.body.registered : null;
        (request.body.owner != null) ? data.owner = request.body.owner : null;

        var commit = new commits(data);
        commit.save(function (err, doc) {
            if(!err){
                resMsgs.onMessage_Response(0,20000,function(res){
                    var resData = res;
                    resData.data = doc;
                    response.status(200).json(resData);
                    utils.writeLog(command, request, startTime, res, err)
                });
            }else{
                resMsgs.onMessage_Response(0,50003,function(res){
                    response.status(500).json(res);
                    utils.writeLog(command, request, startTime, res, err)
                });
            }
        });
    }catch (err) {
        resMsgs.onMessage_Response(0,40300,function(res){
            response.status(403).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}

exports.onEvent_Update = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onEvent_Create";
    try {
        var query = {};
        (request.params.id != null) ? query.code = request.params.id : null;

        var data = {};
        (request.body.title != null) ? data.title = request.body.title : null;
        (request.body.subtitle != null) ? data.subtitle = request.body.subtitle : null;
        (request.body.description != null) ? data.description = request.body.description : null;
        // data.code = utils.randomString(4)+startTime;

        (request.body.dateTime != null) ? data.dateTime = request.body.dateTime : null;

        (request.body.onlyRegistered != null) ? data.onlyRegistered = request.body.onlyRegistered : null;
        (request.body.maxRegistered != null) ? data.maxRegistered = request.body.maxRegistered : null;
        (request.body.notification != null) ? data.notification = request.body.notification : null;
        (request.body.registered != null) ? data.registered = request.body.registered : null;
        (request.body.owner != null) ? data.owner = request.body.owner : null;

        commits
            .findOneAndUpdate(query,data,{new:true})
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0, 50002, function (res) {
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0, 40100, function (res) {
                            response.status(401).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            })
    }catch (err) {
        resMsgs.onMessage_Response(0,40300,function(res){
            response.status(403).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}

exports.onEvent_UpdateAddOnwer = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onEvent_Create";
    try {
        var query = {};
        (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;

        var data = {};
        var owner = [];
        (request.body.owner != null)? owner.$each = request.body.owner : null;


        var push = {};
        (request.body.owner != null)? push.owner = owner : null;
        data.$push = push;

        commits
            .findOneAndUpdate(query,data,{new:true})
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0, 50002, function (res) {
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0, 40100, function (res) {
                            response.status(401).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            })
    }catch (err) {
        resMsgs.onMessage_Response(0,40300,function(res){
            response.status(403).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}


exports.onEvent_DeleteOnwer = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onEvent_Create";
    try {

        var query = {};
        (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;

        var data = {};

        var pull = {};
        (request.body.owner != null)? pull.owner = request.body.owner : null;
        data.$pull = pull;


        commits
            .findOneAndUpdate(query,data,{new:true})
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0, 50002, function (res) {
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0, 40100, function (res) {
                            response.status(401).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            })
    }catch (err) {
        resMsgs.onMessage_Response(0,40300,function(res){
            response.status(403).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}




exports.onEvent_UpdateRegistered = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onEvent_Create";
    try {
        var query = {};
        (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;

        var data = {};
        var registered = [];
        (request.body.registered != null)? registered.$each = request.body.registered : null;


        var push = {};
        (request.body.registered != null)? push.registered = registered : null;
        data.$push = push;

        commits
            .findOneAndUpdate(query,data,{new:true})
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0, 50002, function (res) {
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0, 40100, function (res) {
                            response.status(401).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            })
    }catch (err) {
        resMsgs.onMessage_Response(0,40300,function(res){
            response.status(403).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}


exports.onEvent_CheckRegister = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onEventRegisters";
    try {
        var query = {};
        (request.params.id != null) ? query.code = request.params.id : null;


        var gte = {};
        gte.$gte = new Date().toISOString();

        var lte = {};
        lte.$lte = new Date().toISOString();

        query['dateTime.regStart'] = gte;
        query['dateTime.regEnd'] = lte;
        query.enable = true;
        query.onlyRegistered = true;

        commits
            .findOne(query)
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0, 50002, function (res) {
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        if(doc.maxRegistered <= doc.registered.length){
                            next();
                        }else{
                            resMsgs.onMessage_Response(0, 40100, function (res) {
                                response.status(401).json(res);
                                utils.writeLog(command, request, startTime, res, err)
                            });
                        }

                    } else {
                        resMsgs.onMessage_Response(0, 40100, function (res) {
                            response.status(401).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            })
    }catch (err) {
        resMsgs.onMessage_Response(0,40300,function(res){
            response.status(403).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}


