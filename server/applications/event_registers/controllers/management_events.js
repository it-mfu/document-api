var utils = require('../../../../helpers/utils');
var configs = require('../../../../config/config');
var resMsgs = require('../../management_messages/controllers/management_messages');
var mongo = require('mongodb');

var event_registeers = require('../models/event_registeers.model');


exports.onEventRegisters = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onEventRegisters";
    try {
        console.log(1);
        var query = {};
        (request.params.id != null) ? query.code = request.params.id : null;

        event_registeers
            .find(query)
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0, 50002, function (res) {
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0, 40100, function (res) {
                            response.status(401).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            })
    }catch (err) {
        resMsgs.onMessage_Response(0,40100,function(res){
            response.status(401).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}

exports.onEventRegisters_Create = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onEventRegisters_Create";
    try {
        var data = {};
        (request.body.title != null) ? data.title = request.body.title : null;
        data.code = startTime;
        (request.body.registers != null)? data.registers = request.body.registers : null;

        var event_registeer = new event_registeers(data);
        event_registeer.save(function (err, doc) {
            if(!err){
                resMsgs.onMessage_Response(0,20000,function(res){
                    var resData = res;
                    resData.data = doc;
                    response.status(200).json(resData);
                    utils.writeLog(command, request, startTime, res, err)
                });
            }else{
                resMsgs.onMessage_Response(0,50003,function(res){
                    response.status(500).json(res);
                    utils.writeLog(command, request, startTime, res, err)
                });
            }
        });
    }catch (err) {
        resMsgs.onMessage_Response(0,40100,function(res){
            response.status(401).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}


exports.onEventRegisters_Update_Register = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onEventRegisters";
    try {

        var query = {};
        (request.params.id != null) ? query._id = request.params.id : null;

        var data = {};
        (request.body.title != null) ? data.title = request.body.title : null;
        (request.body.enable != null) ? data.enable = request.body.enable : null;

        var registers = {};
        (request.body.account != null)? registers.account = request.body.account : null;
        (request.body.name != null)? registers.name = request.body.name : null;
        registers.timeStamp = startTime;

        var push = {};
        push.registers =  registers;
        data.$push = push;

        console.log(data);

        event_registeers
            .findOneAndUpdate(query,data,{new:true})
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0, 50002, function (res) {
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0, 40100, function (res) {
                            response.status(401).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            })
    }catch (err) {
        resMsgs.onMessage_Response(0,40100,function(res){
            response.status(401).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}

