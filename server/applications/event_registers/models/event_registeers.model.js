'use strict';

var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var Events_RegistersSchema  = new Schema({
    title           : {type: String, required: true},
    code            : {type: Number, required: true},
    dateEvent       : {type: Date, required: true},
    registers       : [{
        account		: {type: String, default: null},
        name		: {type: String, default: null},
        timeStamp   : {type: Date, default: null},
    }],
    enable          : {type: Boolean, default: true},
    admin           : [{type: Schema.ObjectId, ref: 'Infomation_Accounts'}],
});

var Events_Registers = mongoose.model('Events_Registers', Events_RegistersSchema, 'Events_Registers');
module.exports = Events_Registers;
