'use strict';

var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var Events_RegistersSchema  = new Schema({
    title           : {type: String, required: true},
    subtitle        : {type: String, default: null},
    description     : {type: String, default: null},
    code            : {type: String, required: true},
    dateTime        : {
        promote     : {type: Date, default: null},
        start       : {type: Date, default: null},
        end         : {type: Date, default: null},
        regStart    : {type: Date, default: null},
        regEnd      : {type: Date, default: null},
    },
    onlyRegistered  : {type: Boolean, default: false},
    maxRegistered   : {type: Number, default: 0},
    notification    : {type: Boolean, default: false},
    registered      : [{
        account		: {type: Schema.ObjectId, ref: 'Infomation_Accounts'},
        fullname    : {type: String, default: null},
        regDate     : {type: Date, default:  Date.now},
        checkIn     : {type: Boolean, default: false},
        // timeStamp   : {type: Date, default:  Date.now},
    }],
    enable          : {type: Boolean, default: true},
    owner           : [{type: Schema.ObjectId, ref: 'Infomation_Accounts'}],

});

var Events_Registers = mongoose.model('Events_Registers', Events_RegistersSchema, 'Events_Registers');
module.exports = Events_Registers;
