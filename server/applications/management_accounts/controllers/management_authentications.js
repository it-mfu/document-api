var utils = require('../../../../helpers/utils');
var configs = require('../../../../config/config');
var resMsgs = require('../../management_messages/controllers/management_messages');
var mongo = require('mongodb');

var infomations_accounts = require('../models/infomations_accounts.model');

exports.checkTokenByUser = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "checkUsername";
    try {
        var query = {};
        (request.headers.x_access_token != null) ? query.x_access_token = request.headers.x_access_token : null;

        infomations_accounts
            .findOne(query)
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0, 50002, function (res) {
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        request.body.server_by = doc._id;
                        next();
                    } else {
                        resMsgs.onMessage_Response(0, 40100, function (res) {
                            response.status(401).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            })
    }catch (err) {
        resMsgs.onMessage_Response(0,40100,function(res){
            response.status(401).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}

exports.onAuthenByUserPass = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onAuthenByUserPass";
    try {
        var query = {};
        (request.body.username != null) ? query.username = request.body.username : null;
        (request.body.password != null) ? query.password = utils.encrypt(request.body.password) : null;
        console.log(query);

        infomations_accounts
            .findOne(query)
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0, 50002, function (res) {
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {

                        utils.createToken(function (err, token) {
                            if(!err){
                                switch (doc.status){
                                    case 0:
                                        resMsgs.onMessage_Response(0,40107,function(res){
                                            response.status(401).json(res);
                                            utils.writeLog(command, request, startTime, res, err)
                                        });
                                        break
                                    case 1:
                                        var update = {};
                                        // update.cokies = request.body.cokies;
                                        update.x_access_token = token;
                                        update.token_expired = utils.getTimeInMsec() + configs.tokenExpired;
                                        update.state = 1;
                                        update.last_login = startTime;

                                        infomations_accounts
                                            .findOneAndUpdate(query, update, { new: true })
                                            .populate('Infomation_UserTypes')
                                            .lean()
                                            .exec(function (err, docs) {
                                                delete docs.password;
                                                delete docs.update;
                                                delete docs.create;
                                                delete docs.last_login;
                                                delete docs.notification_id

                                                if(docs.userinfo.image_profile != null){
                                                    docs.userinfo.image_profile = configs.host.domain+":"+configs.host.port+"/images/profile/"+docs.userinfo.image_profile;
                                                }
                                                if(!err){
                                                    resMsgs.onMessage_Response(0,20000,function(res){
                                                        var resData = res;
                                                        resData.data = docs;
                                                        response.status(200).json(resData);
                                                        utils.writeLog(command, request, startTime, res, err)
                                                    });

                                                }
                                            });
                                        break
                                }
                            }else{
                                resMsgs.onMessage_Response(0,50000,function(res){
                                    response.status(500).json(res);
                                    utils.writeLog(command, request, startTime, res, err)
                                });
                            }
                        });
                    } else {
                        resMsgs.onMessage_Response(0, 40401, function (res) {
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            })
    }catch (err) {
        resMsgs.onMessage_Response(0,40100,function(res){
            response.status(401).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}