var utils = require('../../../../helpers/utils');
var multers = require('../../../../helpers/multer');
var configs = require('../../../../config/config');
var resMsgs = require('../../management_messages/controllers/management_messages');

var multers = require('../../../../helpers/multer');
var mongo = require('mongodb');
var multer = require('multer');

var storage = multers.onConfigMulter(configs.images_part.promotion);
var upload = multer({ storage : storage }).single('image');

var infomations_accounts = require('../models/infomations_accounts.model');

exports.resetpassword = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "checkUsername";
    try {
        console.log(request);
        var query = {};
        // (request.body.server.by != null) ? query.username = request.body.server.by : null;
        (request.headers.x_access_token != null) ? query.x_access_token = request.headers.x_access_token : null;


        var data = {};
        (request.body.password != null) ? data.password = utils.encrypt(request.body.password) : null;


        infomations_accounts
            .findOneAndUpdate(query,data,{new:true})
            .lean()
            .exec(function (err, doc) {
                delete doc.password;
                delete doc.create;
                delete doc.update;

                if (err) {
                    resMsgs.onMessage_Response(0, 50002, function (res) {
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    console.log(doc);
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0, 40301, function (res) {
                            response.status(403).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });

                    }
                }
            })
    }catch (err) {
        resMsgs.onMessage_Response(0,40100,function(res){
            response.status(401).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}

exports.onProfile_Update = function (request, response, next) {

    var startTime = utils.getTimeInMsec();
    var command = "onProfile_Update";
    try {
        var query = {};
        (request.headers.x_access_token != null) ? query.x_access_token = request.headers.x_access_token : null;

        var data = {};

        var update = {};
        (request.body.firstname != null) ? update['userinfo.firstname'] = request.body.firstname : null;
        (request.body.lastname != null) ? update['userinfo.lastname'] = request.body.lastname : null;
        (request.body.image != "") ? update['userinfo.image_profile'] = request.body.image : null;
        (request.body.card_id != null) ? update['userinfo.card_id'] = request.body.card_id : null;
        (request.body.age != null) ? update['userinfo.age'] = request.body.age : null;
        (request.body.birthday != null) ? update['userinfo.birthday'] = request.body.birthday : null;
        (request.body.msisdn != null) ? update['userinfo.msisdn'] = request.body.msisdn : null;
        (request.body.email != null) ? update['userinfo.email'] = request.body.email : null;
        (request.body.lineid != null) ? update['userinfo.lineid'] = request.body.lineid : null;
        (request.body.facebook_id != null) ? update['userinfo.facebook_id'] = request.body.facebook_id : null;
        (request.body.google_id != null) ? update['userinfo.google_id'] = request.body.google_id : null;
        (request.body.address != null) ? update['userinfo.address.address'] = request.body.address : null;
        (request.body.zipcode != null) ? update['userinfo.address.zipcode'] = request.body.zipcode : null;

        data.$set = update;

        console.log(data);

        infomations_accounts
            .findOneAndUpdate(query,data,{new:true})
            .lean()
            .exec(function (err, doc) {

                delete doc.password;
                delete doc.create;
                delete doc.update;

                if (err) {
                    resMsgs.onMessage_Response(0, 50002, function (res) {
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0, 40301, function (res) {
                            response.status(403).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            })
    }catch (err) {
        resMsgs.onMessage_Response(0,40100,function(res){
            response.status(401).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}


exports.onProfileNotification_Update = function (request, response, next) {

    var startTime = utils.getTimeInMsec();
    var command = "onProfile_Update";
    try {
        var query = {};
        (request.headers.x_access_token != null) ? query.x_access_token = request.headers.x_access_token : null;

        var data = {};

        var update = {};
        (request.body.notification.platform != null) ? update['notification.platform'] = request.body.notification.platform : null;
        (request.body.notification.pns_token != null) ? update['notification.pns_token'] = request.body.notification.pns_token : null;
        (request.body.notification.pns_key != null) ? update['notification.pns_key'] = request.body.notification.pns_key : null;
        (request.body.notification.pns_packge != null) ? update['notification.pns_packge'] = request.body.notification.pns_packge : null;
        (request.body.notification.enable != null) ? update['notification.enable'] = request.body.notification.enable : null;
        data.$set = update;
        console.log(data);

        infomations_accounts
            .findOneAndUpdate(query,data,{new:true})
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0, 50002, function (res) {
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(20000).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0, 40301, function (res) {
                            response.status(403).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            })
    }catch (err) {
        resMsgs.onMessage_Response(0,40100,function(res){
            response.status(401).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}


exports.onUpload_Images = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = 'onUpload_Images';

    upload(request, response, function (err) {
        request.body.image = (request.file != null) ? request.file.filename : "";
        return next();
    });
}
