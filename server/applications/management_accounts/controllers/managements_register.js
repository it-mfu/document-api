var utils = require('../../../../helpers/utils');
var configs = require('../../../../config/config');
var multers = require('../../../../helpers/multer');
var resMsgs = require('../../management_messages/controllers/management_messages');
var mongo = require('mongodb');
var multer = require('multer');

var storage = multers.onConfigMulter(configs.images_part.profile);
var upload = multer({ storage : storage }).single('image');

var infomations_accounts = require('../models/infomations_accounts.model');

exports.checkUsername = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "checkUsername";
    try {
        var query = {};
        (request.params.id != null) ? query.username = request.params.id : null;

        infomations_accounts
            .findOne(query)
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0, 50002, function (res) {
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0, 40301, function (res) {
                            response.status(403).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            })
    }catch (err) {
        resMsgs.onMessage_Response(0,40100,function(res){
            response.status(401).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}

exports.checkMsisdn = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "checkMsisdn";
    try {
        var userinfo = {};
        (request.params.id != null) ? userinfo.msisdn = request.params.id : null;

        var query = {};
        query.userinfo = userinfo;

        infomations_accounts
            .findOne(query)
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0, 50002, function (res) {
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0, 40301, function (res) {
                            response.status(403).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            })
    }catch (err) {
        resMsgs.onMessage_Response(0,40100,function(res){
            response.status(401).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}

exports.checkEmail = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "checkEmail";
    try {
        var userinfo = {};
        (request.params.id != null) ? userinfo.email = request.params.id : null;

        var query = {};
        query.userinfo = userinfo;

        infomations_accounts
            .findOne(query)
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0, 50002, function (res) {
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0, 40301, function (res) {
                            response.status(403).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            })
    }catch (err) {
        resMsgs.onMessage_Response(0,40100,function(res){
            response.status(401).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}

exports.checkFacebook_id = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "checkEmail";
    try {
        var userinfo = {};
        (request.params.id != null) ? userinfo.facebook_id = request.params.id : null;

        var query = {};
        query.userinfo = userinfo;

        infomations_accounts
            .findOne(query)
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0, 50002, function (res) {
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0, 40301, function (res) {
                            response.status(403).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            })
    }catch (err) {
        resMsgs.onMessage_Response(0,40100,function(res){
            response.status(401).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}

exports.checkGoogle_id = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "checkEmail";
    try {
        var userinfo = {};
        (request.params.id != null) ? userinfo.google_id = request.params.id : null;

        var query = {};
        query.userinfo = userinfo;

        infomations_accounts
            .findOne(query)
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0, 50002, function (res) {
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0, 40301, function (res) {
                            response.status(403).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            })
    }catch (err) {
        resMsgs.onMessage_Response(0,40100,function(res){
            response.status(401).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}

exports.onAccount_CheckRegister = function (request, response, next) {

    var startTime = utils.getTimeInMsec();
    var command = "onAccount_Create";
    try {

        var query = {};
        query.$or = [
            {'username':request.body.username},
            {'userinfo.email':request.body.email},
            {'userinfo.msisdn':request.body.msisdn}
        ];

        infomations_accounts
            .findOne(query)
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0, 50002, function (res) {
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0, 40301, function (res) {
                            response.status(403).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        // resMsgs.onMessage_Response(0,20000,function(res){
                        //     var resData = res;
                        //     resData.data = doc;
                        //     response.status(200).json(resData);
                        //     utils.writeLog(command, request, startTime, res, err)
                        // });
                        next();
                    }
                }
            })

    }catch (err) {
        resMsgs.onMessage_Response(0,40100,function(res){
            response.status(401).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}

exports.onAccount_Creates = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onAccount_Create";
    try {
        // var create = {};
        // (request.body.server.by != null)? create.by = request.body.server.by : null;
        // create.date = startTime;

        var data = {};
        (request.body.username != null) ? data.username = request.body.username : null;
        (request.body.password != null) ? data.password = utils.encrypt(request.body.password) : null;
        (request.body.usertype_id != null) ? data.usertype_id = request.body.usertype_id : null;

        var userInfo = {};
        (request.body.firstname != null) ? userInfo.firstname = request.body.firstname : null;
        (request.body.lastname != null) ? userInfo.lastname = request.body.lastname : null;
        (request.body.image != null) ? userInfo.image_profile = request.body.image : null;
        (request.body.card_id != null) ? userInfo.card_id = request.body.card_id : null;
        (request.body.birthday != null) ? userInfo.birthday = request.body.birthday : null;
        (request.body.msisdn != null) ? userInfo.msisdn = request.body.msisdn : null;
        (request.body.email != null) ? userInfo.email = request.body.email : null;
        (request.body.lineid != null) ? userInfo.lineid = request.body.lineid : null;
        (request.body.facebook_id != null) ? userInfo.facebook_id = request.body.facebook_id : null;
        (request.body.google_id != null) ? userInfo.google_id = request.body.google_id : null;

        data.userinfo = userInfo;

        var infomations_account = new infomations_accounts(data);
        infomations_account.save(function (err, doc) {
            if(!err){
                resMsgs.onMessage_Response(0,20000,function(res){
                    var resData = res;
                    resData.data = doc;
                    response.status(200).json(resData);
                    utils.writeLog(command, request, startTime, res, err)
                });
            }else{
                resMsgs.onMessage_Response(0,50003,function(res){
                    response.status(500).json(res);
                    utils.writeLog(command, request, startTime, res, err)
                });
            }
        });

    }catch (err) {
        resMsgs.onMessage_Response(0,40100,function(res){
            response.status(401).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}


exports.onUpload_Images = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = 'onUpload_Images';

    upload(request, response, function (err) {
        request.body.image = (request.file != null) ? request.file.filename : "";
        return next();
    });
}