var utils = require('../../../../helpers/utils');
var infomation_messages = require('../models/infomation_messages.model');
var resMsgs = require('../../management_messages/controllers/management_messages');
var mongo = require('mongodb');

exports.onMessages = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onMessages";
    try {
        var query = {};
        (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;

        infomation_messages
            .find(query)
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,50003,function(res){
                            response.status(500).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });
    } catch (err) {
        resMsgs.onResponse_Messages(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};

exports.onMessage_Create = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onMessage_Create";
    try {
        var create = {};
        (request.body.server.by != null)? create.by = request.body.server.by : null;
        create.date = startTime;

        var data = {};

        (request.body.api_id != null)? data.api_id = request.body.api_id : null;
        (request.body.code != null)? data.code = request.body.code : null;
        (request.body.message != null)? data.message = request.body.message : null;

        var infomation_message = new infomation_messages(data);
        infomation_message.save(function (err, doc) {
            if (!err) {
                resMsgs.onMessage_Response(0,20000,function(res){
                    var resData = res;
                    resData.data = doc;
                    response.status(200).json(resData);
                    utils.writeLog(command, request, startTime, res, err)
                });
            } else {
                resMsgs.onMessage_Response(0,50003,function(res){
                    response.status(500).json(res);
                    utils.writeLog(command, request, startTime, res, err)
                });
            }
        });

    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            // response.status(500).json(res);
            // utils.writeLog(command, request, startTime, res, err)
        });
    }
};

exports.onMessage_Update = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onMessage_Update";
    try {
        var query = {};
        (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;

        var data = {};
        (request.body.api_id != null)? data.api_id = request.body.api_id : null;
        (request.body.code != null)? data.code = request.body.code : null;
        (request.body.message != null)? data.message = request.body.message : null;

        var update = {};
        (request.body.server.by != null)? update.by = request.body.server.by : null;
        update.date = startTime;

        var push = {};
        push.update =  update;
        data.$push = push;

        infomation_messages
            .findOneAndUpdate(query, data, { new: true })
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0,50002,function(res){
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0,50003,function(res){
                            response.status(500).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            });
    } catch (err) {
        resMsgs.onMessage_Response(0,50000,function(res){
            response.status(500).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
};

exports.onMessage_Response = function (api_number, code, res) {
    try {
        var query = {};
        (api_number != null)? query.api_number = api_number : query.api_number = 0;
        (code != null)? query.code = code : null;
        console.log(query);
        infomation_messages
            .findOne(query)
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    res(err);
                } else {
                    if (doc != null) {
                        console.log(doc);
                        delete doc._id;
                        delete doc.create;
                        delete doc.update;
                        res(doc);
                    } else {
                        res(err);
                    }
                }
            });

    } catch (err) {
        res(err);
    }
};
