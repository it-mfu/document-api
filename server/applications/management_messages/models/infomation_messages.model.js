'use strict';

var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var Infomation_MessagesSchema  = new Schema({
    api_number      : {type: Number, required: true},
    code            : {type: Number, required: true},
    message         : {
        th		    : {type: String, default: null},
        en		    : {type: String, default: null}
    },
    enable          : {type: Boolean, default: true},
    create          : {
        by          : {type: Schema.ObjectId, ref: 'Infomation_Accounts'},
        date        : {type: Date, required: true}
    },
    update          : [{
        by          : {type: Schema.ObjectId, ref: 'Infomation_Accounts'},
        date        : {type: Date, default: null}
    }]

});

var Infomation_Messages = mongoose.model('Infomation_Messages', Infomation_MessagesSchema, 'Infomation_Messages');
module.exports = Infomation_Messages;
