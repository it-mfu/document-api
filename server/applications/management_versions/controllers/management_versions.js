var utils = require('../../../../helpers/utils');
var resMsgs = require('../../management_messages/controllers/management_messages');
var mongo = require('mongodb');

var Infomation_Versions = require('../../management_versions/models/infomation_versions');

exports.onVersions = function (request, response, next) {
    var startTime = utils.getTimeInMsec();
    var command = "onVersions";
    try {
        var query = {};
        // (request.params.id != null) ? query._id = new mongo.ObjectID(request.params.id) : null;
        (request.params.platform != null) ? query.platform = request.params.platform : null;

        Infomation_Versions
            .find(query)
            .lean()
            .exec(function (err, doc) {
                if (err) {
                    resMsgs.onMessage_Response(0, 50002, function (res) {
                        response.status(500).json(res);
                        utils.writeLog(command, request, startTime, res, err)
                    });
                } else {
                    if (doc != null) {
                        resMsgs.onMessage_Response(0,20000,function(res){
                            var resData = res;
                            resData.data = doc;
                            response.status(200).json(resData);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    } else {
                        resMsgs.onMessage_Response(0, 40401, function (res) {
                            response.status(404).json(res);
                            utils.writeLog(command, request, startTime, res, err)
                        });
                    }
                }
            })
    }catch (err) {
        resMsgs.onMessage_Response(0,40100,function(res){
            response.status(401).json(res);
            utils.writeLog(command, request, startTime, res, err)
        });
    }
}
