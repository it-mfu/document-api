var utils = require('../../helpers/utils');
var resMsgs = require('../applications/management_messages/controllers/management_messages');


/* Controller */
var management_versions  =  require('../applications/management_versions/controllers/management_versions');

var management_register = require('../applications/management_accounts/controllers/managements_register');
var management_profile = require('../applications/management_accounts/controllers/management_profile');
var management_authentications = require('../applications/management_accounts/controllers/management_authentications');
var management_events = require('../applications/event_registers/controllers/management_events');



// var management_verifications = require('../applications/management_verifications/controllers/management_register_verifications');

module.exports = function (app) {
    var path = '/api/v1/';

    /*----- Version Controll API true-----*/
    app.get(path + 'versions', management_versions.onVersions);


    app.get(path + 'events',management_authentications.checkTokenByUser, management_events.onEventRegisters);
    app.post(path + 'events',management_authentications.checkTokenByUser, management_events.onEventRegisters_Create);
    app.put(path + 'events/:id',management_authentications.checkTokenByUser, management_events.onEventRegisters_Update_Register);

    /*----- User Register Controll API -----*/
    app.get(path + 'register/check/username/:id', management_register.checkUsername);
    app.get(path + 'register/check/email/:id', management_register.checkEmail);
    app.get(path + 'register/check/msisdn/:id', management_register.checkMsisdn);
    app.get(path + 'register/check/facebook/:id', management_register.checkFacebook_id);
    app.get(path + 'register/check/google/:id', management_register.checkGoogle_id);

    app.post(path + 'register', management_register.onUpload_Images, management_register.onAccount_CheckRegister, management_register.onAccount_Creates);

    /*----- User Verification Controll APIc -----*/
    // app.get(path + 'request/code/email/:id',  management_verifications.onRequest_CodeRegister);
    // app.post(path + 'confirm/code/', management_verifications.onConfirm_CodeRegister);

    /*----- User Authetication Controll API -----*/
    app.post(path + 'login/by/username', management_authentications.onAuthenByUserPass);




    /*----- Web Service -----*/

    // app.get('/authen', function(req, res) {
    //     res.render('doc_api/students/authen', { title: 'Authen' });
    // });
    //
    // app.get('/', function(req, res) {
    //     res.render('doc_api/students/infomation', { title: 'Register Dorm' });
    // });

    app.get('/launcher', function(req, res) {
        res.render('doc_api/admins/launcher', { title: 'DOC API' });
    });

    /*----- Catch 404 Error -----*/
    app.use(function (req, respont , next) {
        var startTime = utils.getTimeInMsec();
        resMsgs.onMessage_Response(0, 40400, function (res) {
            respont.status(404).json(res);
            // utils.writeLog('Client Request', req, startTime, res, null);
            // next();
        });
    });



    /*----- Catch 500 Error -----*/
    app.use(function (err, req, respont) {
        var startTime = utils.getTimeInMsec();
        resMsgs.onMessage_Response(0, 50000, function (res) {
            respont.status(500).json(res);
            // utils.writeLog('Client Request', req, startTime, res, err)
        });
        // var resData = resMsg.getMsg(50000);
        // res.status(500).json(resData);
        // utils.writeLog('Client Request', req, utils.getTimeInMsec(), '----- Request Error URL[' + req.url + '] Status[' + JSON.stringify(resMsg.getMsg(50001)) + '] -----', err);
    });

};
