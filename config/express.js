/**
 * Created by atthapok on 24/06/2559.
 */

var express = require('express');
var bodyParser = require('body-parser');
var nocache = require('nocache');
var nosniff = require('dont-sniff-mimetype');
var frameguard = require('frameguard');
var xssFilter = require('x-xss-protection');
var csp = require('helmet-csp');
var ienoopen = require('ienoopen');
var validator = require('express-validator');
var serveStatic = require('serve-static');
var compression = require('compression');
var morgan = require('morgan')
var sass = require('node-sass-middleware');
var path = require('path');

var initialize = require('../helpers/initialize');
var logger = require('../helpers/logger');

var engine = require('ejs-locals');

module.exports = function () {
    var app = express();

    initialize.init(function (status) {
        if (status) {

            // view engine setup
            app.set('views', path.join(__dirname, '../public/views'));
            app.engine('ejs', engine);
            app.set('view engine', 'ejs');

            app.use(bodyParser.json());
            app.use(bodyParser.urlencoded({extended: false}));
            // app.use(compression());
            app.use(morgan('dev'));
            app.disable('x-powered-by');
            app.use(ienoopen());
            app.use(nocache());
            app.use(nosniff());
            app.use(frameguard());
            app.use(xssFilter());
            app.use(validator());

            app.use(sass({
                src: './sass',
                dest: './public/css',
                debug: true,
                outputStyle: 'compressed',
            }));
            // app.use(express.static('./public'));
            app.use(express.static(path.join(__dirname, './public')));
            app.use(serveStatic('public', {'index': ['index.html', 'index.htm']}));
            // app.use(csp({
            //     // Specify directives as normal.
            //     directives: {
            //         defaultSrc: ["'self'", 'default.com'],
            //         scriptSrc: ["'self'", "'unsafe-inline'"],
            //         styleSrc: ['style.com'],
            //         imgSrc: ['img.com', 'data:'],
            //         sandbox: ['allow-forms', 'allow-scripts'],
            //         reportUri: '/report-violation',
            //
            //         objectSrc: [], // An empty array allows nothing through
            //     },
            //
            //     // Set to true if you only want browsers to report errors, not block them
            //     reportOnly: false,
            //
            //     // Set to true if you want to blindly set all headers: Content-Security-Policy,
            //     // X-WebKit-CSP, and X-Content-Security-Policy.
            //     setAllHeaders: false,
            //
            //     // Set to true if you want to disable CSP on Android where it can be buggy.
            //     disableAndroid: false,
            //
            //     // Set to false if you want to completely disable any user-agent sniffing.
            //     // This may make the headers less compatible but it will be much faster.
            //     // This defaults to `true`.
            //     browserSniff: true
            //
            // }));



            app.use(function(req, res, next) {
                if (req.method === 'OPTIONS') {
                    var headers = {};
                    // IE8 does not allow domains to be specified, just the *
                    // headers["Access-Control-Allow-Origin"] = req.headers.origin;
                    headers["Access-Control-Allow-Origin"] = "*";
                    headers["Access-Control-Allow-Methods"] = "POST, GET, PUT, DELETE, OPTIONS";
                    headers["Access-Control-Allow-Credentials"] = false;
                    headers["Access-Control-Max-Age"] = '86400'; // 24 hours
                    // headers["Access-Control-Allow-Origin", "*";
                    headers["Access-Control-Allow-Headers"] = "X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, Authorization,Access-Control-Allow-Headers, x-access-token,x_access_token, projectid, projectkey";
                    res.writeHead(200, headers);
                    res.end();
                } else {
                    res.header("Access-Control-Allow-Origin", "*");
                    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization,Access-Control-Allow-Headers, x-access-token,x_access_token, projectid, projectkey");
                    next();
                }
            });

            require('../server/routes/app.routes')(app);

        }else{
            logger.error('----- Initialized database Status[Error] -----');
        }
    });
    return app;
};
